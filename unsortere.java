import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Object;
import java.util.ArrayList;

class Unsortere {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> numbers = new ArrayList<>();

        for (String line = br.readLine(); line != null; line = br.readLine()) {


            int x = Integer.parseInt(line);

			if (x == -1) {
				break;
			}

            numbers.add(x);
        }

		int size = numbers.size();

		printmiddle(0,size-1,numbers);
    }

    static void printmiddle(int left, int right, ArrayList<Integer> num) {
		if (left <= right) {
			int middle=(left+right)/2;
			System.out.println(num.get(middle));
			printmiddle(middle+1, right, num);
			printmiddle(left,middle-1, num);
		
			}
    }
}
